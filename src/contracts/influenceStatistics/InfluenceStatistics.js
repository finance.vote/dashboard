import './InfluenceStatistics.css';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Table, Card, Statistic } from 'antd';

function InfluenceStatistics() {
  const [influenceData, setInfluenceData] = useState(); 

  useEffect(() => {
    (async () => {
      await getData();
    })();
  }, []);

  const getData = async () => {
    try{
      const stats = await axios.get(`/api/stats`);
      setInfluenceData(stats.data);
    } catch (err) {
      console.log(err);
    }
  }

  const columns = [
    {
      title: 'Score',
      dataIndex: 'score',
      key: 'score',
      align: 'center',
    },
    {
      title: 'Proposals count',
      dataIndex: 'proposalsCount',
      key: 'proposalsCount',
      align: 'center',
    },
    {
      title: 'Active proposals count',
      dataIndex: 'activeProposalsCount',
      key: 'activeProposalsCount',
      align: 'center',
    },
    {
      title: 'Closed proposals count',
      dataIndex: 'closedProposalsCount',
      key: 'closedProposalsCount',
      align: 'center',
    },
    {
      title: 'Voters count',
      dataIndex: 'votersCount',
      key: 'votersCount',
      align: 'center',
    },
  ];

  return (
    <div className="InfluenceStatistics">
      <Card headStyle={{ color: '#1890ff' }} title={'Total score'}>
        <Statistic value={influenceData?.totalScore} />
      </Card>
      <Card headStyle={{ color: '#1890ff' }} title={'Total proposals'}>
        <Statistic value={influenceData?.totalProposals} />
      </Card>
      <Card headStyle={{ color: '#1890ff' }} title={'Total active proposals'}>
        <Statistic value={influenceData?.totalActiveProposals} />
      </Card>
      <Card headStyle={{ color: '#1890ff' }} title={'Total active proposals'}>
        <Statistic value={influenceData?.totalClosedProposals} />
      </Card>
      <Card headStyle={{ color: '#1890ff' }} title={'Realms'}>
      {
        influenceData?.realms.map((realm, index) => 
          <Card key={index} title={realm.name}>
            <Table columns={columns} dataSource={[realm]} pagination={false}/>
          </Card>)
      }
      </Card>
    </div>
  )
}

export default InfluenceStatistics;
