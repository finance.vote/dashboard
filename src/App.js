import './App.css';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import stats from './stats.json';
import Stat from './Stat'
import { Card, Button, Modal } from 'antd';
import { useState } from 'react';
import InfluenceStatistics from './contracts/influenceStatistics/InfluenceStatistics';
console.log(stats)

const exploreLinks = {
  1: 'https://etherscan.io/address/',
  56: 'https://bscscan.com/address/',
  137: 'https://polygonscan.com/address/',
}

function App() {
  const [isModalVisible, setIsModalVisible] = useState(false);

  return (
    <div className="App">
      <Modal 
        width={1000} 
        footer={null}
        title="Influence statistics" 
        visible={isModalVisible} 
        onCancel={() => setIsModalVisible(false)}>
        <InfluenceStatistics/>
      </Modal>
      <div className="buttonContainer">
        <Button size="large" type="primary" onClick={() => setIsModalVisible(true)} className="button">Influence statistics</Button>
      </div>
      {
        stats.map((stat, index) => (
          <Card key={index} title={<a href={exploreLinks[stat.networkId] + stat.targetAddress}>{stat.targetName}</a>}>
            { stat.tokens.map((token, i) => <Stat key={i} networkId={stat.networkId} targetAddress={stat.targetAddress} tokenName={token.tokenName} tokenAddress={token.tokenAddress} />)}
          </Card>
        ))
      }
    </div>
  )
}

export default App;
